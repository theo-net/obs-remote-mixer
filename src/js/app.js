import React, { useState } from 'react';
import { render } from 'react-dom';
import NoSleep from 'nosleep.js';
import OBSWebSocket from 'obs-websocket-js';
import Atem from './Atem';

function Header ({ connected }) {

    return <header>
        <h1>OBS Remote Mixer</h1>
        <p id="status" className={ connected ? '' : 'disconnected' }>{ connected ? 
            <>Conn<span className="tiny-hidden">ected</span></>
            : <>Disc<span className="tiny-hidden">onnected</span></>            
        } </p>
    </header>;
}


function Topbar ({ connected, OBSDisconnect, obsInfos, sceneName }) {

    const [state, setState] = useState({ wakeLockEnabled: false, fullscreen: false }); 

    /**
     * No sleep mode (disactive the screen saver)
     */
    const noSleep = new NoSleep();
    const noSleepToggle = function () {

        if (!state.wakeLockEnabled) {
            noSleep.enable(); // keep the screen on!
            setState(s => ({ ...s, wakeLockEnabled: true }));
        } else {
            noSleep.disable(); // let the screen turn off.
            setState(s => ({ ...s, wakeLockEnabled: false }));
        }
    }

    /**
     * Fullscreen mode
     */
    const fullscreen = function () {

        if (!state.fullscreen) {

            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen()
            } else if (document.documentElement.msRequestFullscreen) {
                document.documentElement.msRequestFullscreen()
            } else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen()
            } else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen()
            }
            setState(s => ({ ...s, fullscreen: true }));
        }
        else {

            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) { 
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) { 
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) { 
                document.msExitFullscreen();
            }
            setState(s => ({ ...s, fullscreen: false }));
        }
    };

    return <section id="topbar">
        { connected && <button id= "disconnection" onClick={ OBSDisconnect }>Disconnect</button> }        
        { connected && 
        <p id="onair">
            <em>{ sceneName }</em>
            { obsInfos.onAir && obsInfos.rec ?  
                <strong><i></i>On air &amp; Rec</strong> 
                : (obsInfos.onAir ? <strong><i></i>On air</strong> 
                    : (obsInfos.rec ? <strong><i></i>Rec</strong> : '')) }
        </p> }
        <div id="utils">
            <button id="nosleep" className={ state.wakeLockEnabled ? '' : 'disable' } onClick={ noSleepToggle }>No sleep</button>
            <button className={ state.fullscreen ? '' : 'disable' } onClick={ fullscreen }>Fullscreen</button>
        </div>
    </section>;
}


class Connection extends React.Component {

    constructor (props) {

        super(props);
        this.state = {
            server: props.Server,
            port: props.Port,
            password: props.Password
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (e) {

        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit (e) {

        e.preventDefault();
        this.props.OBSConnect(false, this.state.server, this.state.port, this.state.password);
    }

    render () {

        return <section id="connection">
            <h2>You are not connected to OBS Studio</h2>
            <form onSubmit={ this.handleSubmit }>
                <div>
                    <label htmlFor="server">Server</label>
                    <input type="text" name="server" id="server" placeholder="localhost" value={ this.state.server } onChange={ this.handleChange } />
                    <label htmlFor="port">Port</label>
                    <input type="text" name="port" id="port" placeholder="4444" value={ this.state.port } onChange={ this.handleChange } />
                    <label htmlFor="password">Password</label>
                    <input type="password" name="password" id="password" placeholder="if authentication is enabled" value={ this.state.password } onChange={ this.handleChange } />
                </div>
                <p><input type="submit" value="Connect to OBS" /></p>
            </form>
        </section>;
    }
}


function App () {

    const [obs] = useState(new OBSWebSocket());
    const [firstLoad, setFirstLoad] = useState(true); 
    const [connected, setConnected] = useState(false);
    const [paramConnections, setParamConnections] = useState({
        server: '',
        port: '',
        password: ''
    });
    const [obsInfos, setObsInfos] = useState({
        onAir: false,
        rec: false,
        sceneName: ''
    });
    const [infos, setInfos] = useState({
        server: '',
        port: '',
        obsVersion: '',
        obsWebsocketVersion: ''
    });

    function OBSConnection (silent, server = localhost, port = 4444, password = '') {
    
        obs.connect({
            address: server + ':' + port,
            password
        }).then(() => {
    
            obs.send('GetVersion').then(function(result) {
                setInfos({
                    server: server,
                    port: port,
                    obsVersion: result.obsStudioVersion,
                    obsWebsocketVersion: result.obsWebsocketVersion
                });
            });

            obs.send('GetCurrentSceneCollection').then(result => {
                setObsInfos({ sceneName: result.scName });
            });

            // Events
            obs.on('error', err => {
                console.error('socket error:', err)
            });
            obs.on('Exiting', data => {
            
                alert("OBS exited");
                setConnected(false);
                setInfos('');
            });
            obs.on('StreamStarted', data => {
                setObsInfos({...obsInfos, onAir: true });
            });
            obs.on('StreamStopped', data => {
                setObsInfos({...obsInfos, onAir: false });
            });
            obs.on('RecordingStarted', data => {
                setObsInfos({...obsInfos, rec: true });
            });
            obs.on('RecordingStopped', data => {
                setObsInfos({...obsInfos, rec: false });
            });
            obs.on('SceneCollectionChanged', data => {
                obs.send('GetCurrentSceneCollection').then(result => {
                    setObsInfos({ sceneName: result.scName });
                });
            });

            // State
            setParamConnections({ server, port, password });
            setConnected(true);

        }).catch(err => {
    
            console.log(err)
    
            if (err.code == 'CONNECTION_ERROR' && silent === true) {
                if (server !== window.location.hostname)
                    OBSConnection(true, window.location.hostname, '4444');
            }
            else if (err.code == 'CONNECTION_ERROR' && silent != true)
                alert('Please make sure OBS is on, with the plugin "obs-websocket" installed and try to connect');
        });
    }

    function OBSDeconnection () {

        obs.disconnect();
        setConnected(false);
        setInfos('');
    }


    if (firstLoad) {
        OBSConnection(true, 'localhost', '4444', '');
        setFirstLoad(false);
    }

    return <>
        <Header connected={ connected } />
        <Topbar 
            connected={ connected } 
            OBSDisconnect={ OBSDeconnection }
            obsInfos={ obsInfos }
            sceneName={ obsInfos.sceneName} />
        { connected ? 
            <Atem obs={ obs } setObsInfos={ setObsInfos } /> 
            : <Connection OBSConnect={ OBSConnection } Server={ paramConnections.server } Port={ paramConnections.port } Password={ paramConnections.password } /> }
        { connected ? 
            <p id="info">
                Connected to <em>{ infos.server }:{ infos.port } </em> | 
                OBS v{ infos.obsVersion } obs-websocket v{ infos.obsWebsocketVersion }</p>
        : '' }
    </>;
}

render(
    <App />, 
    document.querySelector('#app')
);