import React from 'react';

function _transAnimation (transDuration) {

    let elem = document.getElementById('trans_animation_child');
    let width = 0;
    let debut = new Date().getTime();
    let id = setInterval(frame, 1)
    let largeur = 0;

    function frame () {
        let now = new Date().getTime();
        if (now >= debut + transDuration) {
            clearInterval(id);
            elem.style.width = "100%"
            setTimeout(() => {
                elem.style.width = "0%"
            }, 2000);
        } else {
            largeur = ((now - debut) / transDuration) * 100;
            elem.style.width = largeur + '%'
        }
    }
}

class Program extends React.Component {
    
    constructor (props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick (e) {

        this.props.setCurrentScene(e.target.name);
    }

    render () {
        
        return <div id="program" className="box">
            <h2>Program</h2>
            <div>
                { this.props.sceneList.map(scene => (
                    <button className={ 'buttonscene' + (this.props.currentScene == scene ? ' redbutton' : '') } 
                        key={ 'pgm_' + scene } 
                        onClick={ this.handleClick } 
                        name={ scene }
                    >
                        { scene }
                    </button>
                )) }
            </div>
        </div>;
    }
}


class Transition extends React.Component {
    
    constructor (props) {

        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.switchTrans = this.switchTrans.bind(this);
        this.handleSwitch = this.handleSwitch.bind(this);
    }

    handleChange (e) {

        this.props.setTransitionDuration(parseInt(e.target.value));
    }

    switchTrans (e) {

        this.props.setActiveTransition(e.target.name);
    } 

    handleSwitch () {

        this.props.setCurrentScene(this.props.previewScene);
    }
    
    render () {

        return <div id="transition" className="box">
            <h2>
                Transition style 
                <input type="number" id="transition_input" value={ this.props.transitionDuration } min="0" max="10000" step="50" onChange={ this.handleChange } />
            </h2>
            <div>
                <button className="buttonscene redbutton" id="switch" onClick={ this.handleSwitch }>Switch</button> 
                { this.props.transitionList.map(transition => (
                    <button className={ 'buttonscene' + (transition.name == this.props.activeTransition ? ' yellowbutton' : '') } 
                        key={ 'trans_' + transition.name } 
                        onClick={ this.switchTrans }
                        name={ transition.name }
                    >
                        { transition.name }
                    </button>
                )) }
            </div>
            <section id="lesboutons_TRANS">
                <div id="trans_animation_parent">
                    <div id="trans_animation_child"></div>
                </div>
            </section>
        </div>;
    }
}


class Preview extends React.Component {
    
    constructor (props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick (e) {

        this.props.setPreviewScene(e.target.name);
    }

    render () {

        return <div id="preview" className="box">
            <h2>Preview</h2>
            <div>
                { this.props.sceneList.map(scene => (
                    <button className={ 'buttonscene' + (this.props.previewScene == scene ? ' greenbutton' : '') } 
                        key={ 'pvw_' + scene } 
                        onClick={ this.handleClick }
                        name={ scene }
                    >
                        { scene }
                    </button>
                )) }
            </div>
        </div>;
    }
}


class AudioButton extends React.Component {

    constructor (props) {

        super(props);
        this.state = {
            muted: true
        }
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount () {

        this.checkMuted();
        this.props.obs.on('SourceMuteStateChanged', data => {
            this.checkMuted();
        });
    }

    checkMuted () {

        this.props.obs.send('GetVolume', {source: this.props.name}).then(result => {
            this.setState({ muted: result.muted });
        }).catch(err => { console.log(err); });
    }

    handleClick(e) {

        this.props.obs.send('ToggleMute', {source: this.props.name}).then(() => {
            this.checkMuted();
        });
    }

    render () {

        return <button 
            className={ 'buttonscene' + (this.state.muted ? '' : ' bluebutton')}
            key={ 'audio_' + this.props.name }
            onClick={ this.handleClick }
            name={ this.props.name }>{ this.props.name }</button>;
    }
}

class Mixer extends React.Component {

    constructor (props) {

        super(props);
        this.state = {
            specialSources: []
        };
    }

    componentDidMount () {
    
        this.props.obs.send('GetSpecialSources').then(result => {
    
            let sources = [];
    
            Object.entries(result).forEach(element => {
                
                let name = Object.values(element)[0];
                switch (name) {
                    case 'desktop-1':
                        sources.push(Object.values(element)[1]);
                        break;
                    case 'desktop-2':
                        sources.push(Object.values(element)[1]);
                        break;
                    case 'mic-1':
                        sources.push(Object.values(element)[1]);
                        break;
                    case 'mic-2':
                        sources.push(Object.values(element)[1]);
                        break;
                    case 'mic-3':
                        sources.push(Object.values(element)[1]);
                        break;
                    default:
                        //nothing
                }
            });
            this.setState({ specialSources: sources });
        });
    }

    render () {

        if (!this.props.sourceList[this.props.currentScene])
            this.props.sourceList[this.props.currentScene] = [];

        return <div id="audio" className="box">
            <h2>Mixer</h2>
            <div>
                { this.state.specialSources.map(source => {
                    return <AudioButton obs={ this.props.obs } name={ source } key={ source } />;  
                }) }
                { this.props.sourceList[this.props.currentScene].map(source => {
                    return ((source.type == 'pulse_input_capture' || source.type == 'ffmpeg_source') && source.render) ? 
                        <AudioButton obs={ this.props.obs } name={ source.name } key={ source.name } /> : '';
                }) }
            </div>
        </div>;
    }
}


class SourceBtn extends React.Component {
    
    constructor (props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick () {

        this.props.obs.send('SetSceneItemProperties', 
            { 'scene-name': this.props.previewScene, item: this.props.source.name, visible: !this.props.source.render }
        ).catch(err => { console.log(err); })
    }

    render () {

        return <button 
            className={ 'buttonscene' + (this.props.source.render ? ' bluebutton' : '') } 
            onClick={ this.handleClick } 
            name={ this.props.source.name }
        >
            { this.props.source.name }
        </button>;
    }
}

class Sources extends React.Component {
    
    constructor (props) {

        super(props);
    }

    render () {

        if (!this.props.sourceList[this.props.previewScene])
            this.props.sourceList[this.props.previewScene] = [];

        return <div id="sources" className="box">
            <h2>Sources <span>{ this.props.previewScene }</span></h2>
            <div>
                { this.props.sourceList[this.props.previewScene].map(source => (
                    <SourceBtn 
                        obs={ this.props.obs } 
                        source={ source } 
                        key={ 'src_' + source.name } 
                        previewScene={ this.props.previewScene } />
                )) }
            </div>
        </div>;
    }
}


class Atem extends React.Component {
    
    constructor (props) {

        super(props);

        this.state = {
            sceneList: [],
            sourceList: {},
            currentScene: '',
            previewScene: '',
            transitionList: [],
            activeTransition: '',
            transitionDuration: 500
        };

        this.setTransitionDuration = this.setTransitionDuration.bind(this);
        this.setActiveTransition = this.setActiveTransition.bind(this);
        this.setCurrentScene = this.setCurrentScene.bind(this);
        this.setPreviewScene = this.setPreviewScene.bind(this);
    }

    loadScenes () {

        this.props.obs.send('GetSceneList').then(data => {
        
            let _sceneList = [];
            let _sourceList = {};

            data.scenes.forEach(scene => {
                _sceneList.push(scene.name);
                _sourceList[scene.name] = scene.sources;
            });

            this.setState({ 
                sceneList: _sceneList,
                sourceList: _sourceList,
                currentScene: data.currentScene
            });
        
            this.props.obs.send('GetStreamingStatus').then(result => {
                this.props.setObsInfos(infos => ({ ...infos, onAir: result.streaming, rec: result.recording }));
            });
        
            this.props.obs.send('GetPreviewScene').then(result => {
                this.setState({ previewScene: result.name });
            }).catch(err => { 
                if (err.messageId == 5)
                    alert('OBS need to be in studio mode !')
                console.log(err); 
            });

            this.loadTransitions();
        });
    }

    componentDidMount () {

        this.loadScenes();

        // Events
        this.props.obs.on('TransitionListChanged', data => {
            this.loadTransitions();
        });
        this.props.obs.on('SwitchTransition', data => {
            this.setState({ activeTransition: data.transitionName });
        });
        this.props.obs.on('TransitionDurationChanged', data => {
            this.setState({ transitionDuration: data.newDuration });
        });
        this.props.obs.on('PreviewSceneChanged', data => {
            this.setState({ previewScene: data.sceneName});
        });
        this.props.obs.on('SceneItemVisibilityChanged', data => {

            if (data.sceneName == this.state.previewScene) {

                this.state.sourceList[this.state.previewScene].map(source => {

                    if (source.name === data.itemName) {
                        source.render = data.itemVisible;
                    }
                    return source; 
                });
                this.setState({ sourceList: this.state.sourceList});
            } 
        });
        this.props.obs.on('SwitchScenes', data => {
            this.setState({ currentScene: data.sceneName});
            _transAnimation(this.state.transitionDuration);
        });
        this.props.obs.on('SourceRenamed', data => {
            console.log('SourceRenamed');
            this.loadScenes();
        });
        this.props.obs.on('SourceOrderChanged', data => {
            console.log('SourceOrderChanged');
            this.loadScenes();
        });
        this.props.obs.on('SceneItemAdded', data => {
            console.log('SceneItemAdded');
            this.loadScenes();
        });
        this.props.obs.on('SceneItemRemoved', data => {
            console.log('SceneItemRemoved');
            this.loadScenes();
        });
        this.props.obs.on('SourceDestroyed', data => {
            console.log('SourceDestroyed');
            this.loadScenes();
        });
        this.props.obs.on('SourceCreated', data => {
            console.log('SourceCreated');
            this.loadScenes();
        });
    }

    loadTransitions () {

        this.props.obs.send('GetTransitionList').then(result => {
            this.setState({
                transitionList: result.transitions,
                activeTransition: result.currentTransition
            });
        }).catch(err => { console.log(err); });
        this.props.obs.send('GetTransitionDuration').then(result => {
            this.setState({ transitionDuration: result.transitionDuration });
        }).catch(err => { console.log(err); });
    }

    setActiveTransition (activeTransition) {

        this.props.obs.send('SetCurrentTransition', {'transition-name': activeTransition}).then(() => {
            this.setState({ activeTransition });
        }).catch(err => { console.log(err); });
    }

    setTransitionDuration (transitionDuration) {

        this.props.obs.send('SetTransitionDuration', {
            duration: parseInt(transitionDuration)
        }).catch(err => { console.log(err); });
        this.setState({ transitionDuration });
    }

    setCurrentScene (currentScene) {

        this.props.obs.send('SetCurrentScene', {'scene-name': currentScene}).then(() => {
        }).catch(err => { console.log(err); });
    }

    setPreviewScene (previewScene) {

        this.props.obs.send('SetPreviewScene', {'scene-name': previewScene}).then(() => {
            this.setState({ previewScene });
        }).catch(err => { console.log(err); });
    }

    render () {

        return <section id="atem">

            <Program 
                sceneList={ this.state.sceneList } 
                currentScene={ this.state.currentScene }
                setCurrentScene={ this.setCurrentScene }  />
            <Transition 
                transitionList={ this.state.transitionList } 
                activeTransition={ this.state.activeTransition } 
                setActiveTransition={ this.setActiveTransition } 
                transitionDuration={ this.state.transitionDuration }
                setTransitionDuration={ this.setTransitionDuration } 
                setCurrentScene={ this.setCurrentScene } 
                previewScene={ this.state.previewScene } />
            <Preview 
                sceneList={ this.state.sceneList } 
                previewScene={ this.state.previewScene } 
                setPreviewScene={ this.setPreviewScene } />
            <Mixer 
                obs={ this.props.obs }
                currentScene={ this.state.currentScene }
                sourceList={ this.state.sourceList } />
            <Sources 
                obs={ this.props.obs }
                previewScene={ this.state.previewScene } 
                sourceList={ this.state.sourceList } />
        </section>;
    }
}

export default Atem;
