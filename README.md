OBS Remote Mixer
===

Simple interface to manage OBS with your browser. 

To run this app, you need before to execute:

    npm install

And after:

    npm start
